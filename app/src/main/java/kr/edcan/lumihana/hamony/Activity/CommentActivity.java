package kr.edcan.lumihana.hamony.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.BlurTransformation;
import kr.edcan.lumihana.hamony.Adapter.CommentAdapter;
import kr.edcan.lumihana.hamony.Data.CommentData;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.Utils.IHamonyService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by kimok_000 on 2016-07-16.
 */
public class CommentActivity extends AppCompatActivity {
    private TextView text_title, text_content;
    private EditText edit_comment;
    private ImageView image_send, image_image;
    private ListView list_comments;
    private Intent intent;
    private ArrayList<CommentData.DataBean> arrayList;
    private CommentAdapter adapter;
    private Retrofit retrofit;
    private IHamonyService hamonyService;
    private Call<CommentData.DataBean> comments;
    private SharedPreferences sharedPreferences;

    private boolean isAnonymous;
    private int id;
    private String title, content, comment, image, name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        sharedPreferences = getSharedPreferences("User", MODE_PRIVATE);
        name = sharedPreferences.getString("name", getResources().getString(R.string.text_anonymous));

        intent = getIntent();
        id = intent.getIntExtra("id", -1);
        title = intent.getStringExtra("title");
        image = intent.getStringExtra("image");
        content = intent.getStringExtra("content");
        isAnonymous = intent.getBooleanExtra("isAnonymous", false);

        if(isAnonymous) name = getResources().getString(R.string.text_anonymous);

        list_comments = (ListView) findViewById(R.id.comment_list_comments);
        text_title = (TextView) findViewById(R.id.comment_text_title);
        text_content = (TextView) findViewById(R.id.comment_text_content);
        edit_comment = (EditText) findViewById(R.id.comment_edit_comment);
        image_send = (ImageView) findViewById(R.id.comment_image_send);
        image_image = (ImageView) findViewById(R.id.comment_image_image);
        text_title.setText(title + "");
        text_content.setText(content);

        image_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSend();
            }
        });

        Glide.with(getApplicationContext()).load("http://125.176.34.15:9000" + image)
                .placeholder(R.color.colorPrimary)
                .skipMemoryCache(false)
                .bitmapTransform(new BlurTransformation(getApplicationContext(), 6))
                .crossFade()
                .thumbnail(0.5f)
                .into(image_image);

        initList();
        loadList();
    }

    private void onSend() {
        comment = edit_comment.getText().toString().trim();

        if (comment.equals("")) {
            Toast.makeText(getApplicationContext(), R.string.msg_fill_content, Toast.LENGTH_SHORT).show();
            return;
        }

        writeCommentService();
    }

    private void writeCommentService() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://125.176.34.15:9000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hamonyService = retrofit.create(IHamonyService.class);
        comments = hamonyService.writeComment(id, name, comment);
        comments.enqueue(new Callback<CommentData.DataBean>() {
            @Override
            public void onResponse(Response<CommentData.DataBean> response, Retrofit retrofit) {
                loadList();
                edit_comment.setText("");
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadList() {
        final ProgressDialog progressBar = new ProgressDialog(CommentActivity.this);
        progressBar.show();
        Call<CommentData> comment;

        retrofit = new Retrofit.Builder()
                .baseUrl("http://125.176.34.15:9000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hamonyService = retrofit.create(IHamonyService.class);
        comment = hamonyService.seeComment(id);
        comment.enqueue(new Callback<CommentData>() {
            @Override
            public void onResponse(Response<CommentData> response, Retrofit retrofit) {
                if (response.body().isComment()) {
                    initList();
                    ArrayList<CommentData.DataBean> _arrayList = response.body().getData();
                    for (int i = 0; i < _arrayList.size(); i++) {
                        CommentData.DataBean array = _arrayList.get(i);
                        arrayList.add(array);
                    }
                }
                progressBar.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar.dismiss();
            }
        });
    }

    public void initList() {
        arrayList = new ArrayList<>();
        setList();
    }

    public void setList() {
        adapter = new CommentAdapter(getApplicationContext(), arrayList);
        list_comments.setAdapter(adapter);
    }
}
