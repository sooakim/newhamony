package kr.edcan.lumihana.hamony.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import kr.edcan.lumihana.hamony.Data.UserData;
import kr.edcan.lumihana.hamony.Dialog.RegisterDialog;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.Utils.IHamonyService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by kimok_000 on 2016-07-14.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText edit_id, edit_pw;
    private TextView text_login, text_register;
    private String id, pw;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPreferences = getSharedPreferences("User", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        edit_id = (EditText) findViewById(R.id.login_edit_id);
        edit_pw = (EditText) findViewById(R.id.login_edit_pw);
        text_login = (TextView) findViewById(R.id.login_text_login);
        text_register = (TextView) findViewById(R.id.login_text_register);

        text_login.setOnClickListener(this);
        text_register.setOnClickListener(this);

        if(!sharedPreferences.getString("name", "").equals("")) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_text_login : {
                onLogin();
                break;
            }

            case R.id.login_text_register : {
                onRegister();
                break;
            }
        }
    }

    private void onLogin() {
        id = edit_id.getText().toString().trim();
        pw = edit_pw.getText().toString().trim();

        if(id.equals("") || pw.equals("")){
            Toast.makeText(getApplicationContext(), R.string.msg_fill_content, Toast.LENGTH_SHORT).show();
            return;
        }

        loginService();
    }

    private void loginService() {
        final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
        dialog.setTitle(R.string.msg_loading);
        dialog.setMessage(getResources().getString(R.string.msg_now_loading));
        dialog.show();

        Retrofit retrofit;
        IHamonyService hamonyService;
        Call<UserData> login;

        retrofit = new Retrofit.Builder()
                .baseUrl("http://125.176.34.15:9000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hamonyService = retrofit.create(IHamonyService.class);
        login = hamonyService.login(id, pw);
        login.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Response<UserData> response, Retrofit retrofit) {
                switch (response.code()){
                    case 401 : {
                        Toast.makeText(getApplicationContext(), R.string.msg_account_not_found, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case 402 : {
                        Toast.makeText(getApplicationContext(), R.string.msg_password_not_match, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case 403 : {
                        Toast.makeText(getApplicationContext(), R.string.msg_unkown_error, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case 200 : {
                        dialog.dismiss();
                        UserData.DataBean data = response.body().getDb_result();
                        Log.e("name", data.getName()+"");
                        editor.putString("name", data.getName());
                        editor.commit();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();
                        break;
                    }

                    default: {
                        Toast.makeText(getApplicationContext(), R.string.msg_unkown_error, Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Login", t.getMessage());
                Toast.makeText(getApplicationContext(), R.string.msg_network_error, Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void onRegister() {
        RegisterDialog dialog = new RegisterDialog(LoginActivity.this);
        dialog.show();
    }
}
