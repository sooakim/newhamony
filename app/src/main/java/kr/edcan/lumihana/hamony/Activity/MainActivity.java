package kr.edcan.lumihana.hamony.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.Adapter.PostAdapter;
import kr.edcan.lumihana.hamony.Data.PostData;
import kr.edcan.lumihana.hamony.Dialog.SelectionDialog;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.Utils.IHamonyService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private GridView grid_contents;
    private PostAdapter adapter;
    private ArrayList<PostData.DataBean> arrayList;
    private FloatingActionButton floatingActionButton;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Toolbar toolbar;
    private DrawerLayout main_drawer;
    private ActionBarDrawerToggle drawerToggle;
    private SharedPreferences sharedPreferences;
    private TextView text_profile, text_name, text_last, text_process, text_hot;

    private int listCount = 0;
    private String name;
    private int type = IHamonyService.LIST_TYPE_LAST;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences("User", MODE_PRIVATE);
        name = sharedPreferences.getString("name", getResources().getString(R.string.text_anonymous));

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.main_SRL_refresh);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.main_fab);
        text_profile = (TextView) findViewById(R.id.main_text_profile);
        text_name = (TextView) findViewById(R.id.main_text_name);
        text_last = (TextView) findViewById(R.id.main_text_last);
        text_process = (TextView) findViewById(R.id.main_text_process);
        text_hot = (TextView) findViewById(R.id.main_text_hot);
        main_drawer = (DrawerLayout) findViewById(R.id.main_drawer);
        grid_contents = (GridView) findViewById(R.id.main_grid_contents);
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);

        char ch = name.toCharArray()[0];
        if ((ch >= 'a' && ch <= 'z'))
            text_profile.setText(ch + "".toUpperCase());
        else text_profile.setText(name);
        text_name.setText(name + "");

        drawerToggle = new ActionBarDrawerToggle(this, main_drawer, R.string.app_name, R.string.app_name);
        main_drawer.addDrawerListener(drawerToggle);

        initList();
        loadList();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), PostActivity.class);
                new SelectionDialog(MainActivity.this) {
                    @Override
                    public void onDebate() {
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("type", PostData.TYPE_DEBATE);
                        startActivity(intent);
                    }

                    @Override
                    public void onGoodBad() {
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("type", PostData.TYPE_GBVOTE);
                        startActivity(intent);
                    }

                    @Override
                    public void onVote() {
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("type", PostData.TYPE_VOTE);
                        startActivity(intent);
                    }
                }.show();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        grid_contents.setOnScrollListener(new AbsListView.OnScrollListener() {
            private boolean mLastItemVisible;

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                mLastItemVisible = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount - 1);
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && mLastItemVisible) {
                    loadList();
                }
            }
        });

        Log.e("name", name);
        text_last.setOnClickListener(this);
        text_process.setOnClickListener(this);
        text_hot.setOnClickListener(this);
    }

    private void refreshList() {
        listCount = 0;
        initList();
        setList();
        loadList();
    }

    private void loadList() {
        Retrofit retrofit;
        IHamonyService hamonyService;
        Call<PostData> post;

        retrofit = new Retrofit.Builder()
                .baseUrl("http://125.176.34.15:9000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hamonyService = retrofit.create(IHamonyService.class);
        post = hamonyService.listOfPost(type, listCount + 0);
        post.enqueue(new Callback<PostData>() {
            @Override
            public void onResponse(Response<PostData> response, Retrofit retrofit) {
                switch (response.code()) {
                    case 401: {
                        Log.e("main", "end of post");
                        break;
                    }

                    case 200: {
                        ArrayList<PostData.DataBean> _arrayList = response.body().getData();
                        listCount = _arrayList.size();
                        for (int i = 0; i < _arrayList.size(); i++) {
                            PostData.DataBean array = _arrayList.get(i);
                            Log.e("main", array.getId() + " : " + array.getContent());
                            arrayList.add(array);
                        }
                        adapter.notifyDataSetChanged();
                        break;
                    }
                    default:
                        Toast.makeText(getApplicationContext(), R.string.msg_unkown_error, Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("main", t.getMessage());
                Toast.makeText(getApplicationContext(), R.string.msg_network_error, Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void initList() {
        arrayList = new ArrayList<>();
        setList();
    }

    private void setList() {
        if (arrayList != null) {
            adapter = new PostAdapter(getApplicationContext(), arrayList);
            grid_contents.setAdapter(adapter);
        } else initList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_search:
                startActivity(new Intent(getApplicationContext(), SearchActivity.class));
                break;
        }

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_text_last: {
                type = IHamonyService.LIST_TYPE_LAST;
                refreshList();
                break;
            }

            case R.id.main_text_process: {
                type = IHamonyService.LIST_TYPE_PROGRESS;
                refreshList();
                break;
            }

            case R.id.main_text_hot: {
                type = IHamonyService.LIST_TYPE_SPOT;
                refreshList();
                break;
            }
        }
    }
}
