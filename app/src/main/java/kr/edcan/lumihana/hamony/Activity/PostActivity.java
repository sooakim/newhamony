package kr.edcan.lumihana.hamony.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.edcan.lumihana.hamony.Adapter.TagAdapter;
import kr.edcan.lumihana.hamony.Data.PostData;
import kr.edcan.lumihana.hamony.Data.VoteData;
import kr.edcan.lumihana.hamony.Dialog.VoteDialog;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.Utils.IHamonyService;
import kr.edcan.lumihana.hamony.Utils.RecyclerViewDecoration;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by kimok_000 on 2016-07-17.
 */
public class PostActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView text_image, text_name;
    private ImageView image_image;
    private EditText edit_title, edit_content;
    private RecyclerView recycler_tags;
    private TagAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<String> arrayList;
    private RecyclerViewDecoration decoration;
    private Toolbar toolbar;
    private SharedPreferences sharedPreferences;
    private Intent intent;

    private String name;
    private Uri uriImage;
    private String fileName;
    private Bitmap image;
    private String title, bulletincontent;
    private long endtime;
    private String[] tag, vote;
    private int type = -1;
    private boolean isAnonymous = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        sharedPreferences = getSharedPreferences("User", MODE_PRIVATE);
        name = sharedPreferences.getString("name", getResources().getString(R.string.text_anonymous));
        intent = getIntent();
        type = intent.getIntExtra("type", -1);

        toolbar = (Toolbar) findViewById(R.id.post_toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setTitle(R.string.text_post);
        setSupportActionBar(toolbar);

        text_image = (TextView) findViewById(R.id.post_text_image);
        image_image = (ImageView) findViewById(R.id.post_image_image);
        recycler_tags = (RecyclerView) findViewById(R.id.post_recycler_tags);
        text_name = (TextView) findViewById(R.id.post_text_name);
        edit_title = (EditText) findViewById(R.id.post_edit_title);
        edit_content = (EditText) findViewById(R.id.post_edit_content);
        text_image.setOnClickListener(this);

        layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        decoration = new RecyclerViewDecoration(8);
        recycler_tags.setHasFixedSize(false);
        recycler_tags.addItemDecoration(decoration);
        recycler_tags.setLayoutManager(layoutManager);

        text_name.setText(name + "");
        text_name.setOnClickListener(this);

        edit_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Pattern pattern = Pattern.compile("\\s*(#\\w+\\s+)*");
                Matcher matcher = pattern.matcher(s.toString());
                if (matcher.matches()) {
                    Log.e("hello", s.toString());
                    String[] temp = s.toString().split(" ");
                    for (String str : temp) {
                        if (!str.trim().equals("")) {
                            arrayList.add(str.replace("#", "").trim());
                            edit_content.setText(str.replace(str, ""));
                        }
                    }
                    setList();
                }
            }
        });

        initList();

    }

    private void initList() {
        arrayList = new ArrayList<>();
        setList();
    }

    private void setList() {
        adapter = new TagAdapter(PostActivity.this, arrayList, R.layout.content_post) {
            @Override
            public void onCancel(int position) {
                arrayList.remove(position);
                setList();
            }
        };
        recycler_tags.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.post_text_image: {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
                intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 100);
                break;
            }

            case R.id.post_text_name: {
                if (isAnonymous) {
                    isAnonymous = false;
                    text_name.setText(name);
                } else {
                    isAnonymous = true;
                    text_name.setText(R.string.text_anonymous);
                }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    fileName = getImageNameToUri(data.getData());
                    image = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                    uriImage = data.getData();

                    text_image.setText(fileName);
                    image_image.setImageBitmap(image);
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), R.string.msg_file_not_found, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), R.string.msg_io_error, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), R.string.msg_unkown_error, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void onPost() {
        title = edit_title.getText().toString().trim();
        bulletincontent = edit_content.getText().toString().trim();
        tag = arrayList.toArray(new String[arrayList.size()]);

        if (title.equals("") || bulletincontent.equals("")) {
            Toast.makeText(getApplicationContext(), R.string.msg_fill_content, Toast.LENGTH_SHORT).show();
            return;
        }

        if (endtime < 30000) {
            Toast.makeText(getApplicationContext(), R.string.msg_too_short, Toast.LENGTH_SHORT).show();
            return;
        }

        if(type == PostData.TYPE_VOTE){
            VoteDialog dialog = new VoteDialog(PostActivity.this) {
                @Override
                public void onVote(ArrayList<VoteData> arrayList) {
                    
                }

                @Override
                public void onCancel() {

                }
            }
        }

        postService();
    }

    private void postService() {
        final ProgressDialog dialog = new ProgressDialog(PostActivity.this);
        dialog.setTitle(R.string.msg_loading);
        dialog.setMessage(getResources().getString(R.string.msg_now_loading));
        dialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://125.176.34.15:9000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IHamonyService hamonyService = retrofit.create(IHamonyService.class);

        RequestBody requestBody;
        if (image != null) {
            MediaType MEDIA_TYPE_PNG = MediaType.parse("image/jpeg");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] data = stream.toByteArray();

            Log.e("data", data.toString());

            requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart("photo", fileName, RequestBody.create(MEDIA_TYPE_PNG, data))
                    .build();
        } else {
            requestBody = null;
        }

        Call<PostData> post = hamonyService.writePost(title, name, bulletincontent, isAnonymous, type, tag, endtime, requestBody);
        post.enqueue(new Callback<PostData>() {
            @Override
            public void onResponse(Response<PostData> response, Retrofit retrofit) {
                switch (response.code()) {
                    case 200: {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), R.string.msg_post_success, Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    }
                    default: {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), R.string.msg_unkown_error, Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("debate", t.getMessage());
                Toast.makeText(getApplicationContext(), R.string.msg_network_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getImageNameToUri(Uri data) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(data, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        String imgPath = cursor.getString(column_index);
        String imgName = imgPath.substring(imgPath.lastIndexOf("/") + 1);

        return imgName;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.post_post: {
                onPost();
                break;
            }

            case R.id.post_timer: {
                Date date = new Date();
                TimePickerDialog dialog = new TimePickerDialog(PostActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        endtime = (long) (hourOfDay * 60 + minute) * 1000;
                    }
                }, date.getHours(), date.getMinutes(), true);
                dialog.show();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}