package kr.edcan.lumihana.hamony.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.Adapter.PostAdapter;
import kr.edcan.lumihana.hamony.Data.PostData;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.Utils.IHamonyService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by kimok_000 on 2016-07-16.
 */
public class SearchActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewPager pager_content;
    private ImageView image_back, image_clear, image_search, image_title, image_tag;
    private RelativeLayout relative_title, relative_tag;
    private EditText edit_search;
    private ArrayList<PostData.DataBean> arrayList;
    private PostAdapter adapter;
    private GridView grid_contents;

    private int tabPosition = 0;
    private int listCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        image_back = (ImageView) findViewById(R.id.search_image_back);
        grid_contents = (GridView) findViewById(R.id.search_grid_contents);
        image_clear = (ImageView) findViewById(R.id.search_image_clear);
        image_search = (ImageView) findViewById(R.id.search_image_search);
        edit_search = (EditText) findViewById(R.id.search_edit_search);
        image_title = (ImageView) findViewById(R.id.search_image_title);
        image_tag = (ImageView) findViewById(R.id.search_image_tag);
        relative_title = (RelativeLayout) findViewById(R.id.search_relative_title);
        relative_tag = (RelativeLayout) findViewById(R.id.search_relative_tag);

        image_back.setOnClickListener(this);
        image_clear.setOnClickListener(this);
        image_search.setOnClickListener(this);
        image_title.setOnClickListener(this);
        image_tag.setOnClickListener(this);
        relative_tag.setOnClickListener(this);
        relative_title.setOnClickListener(this);

        grid_contents.setOnScrollListener(new AbsListView.OnScrollListener() {
            private boolean mLastItemVisible;

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                mLastItemVisible = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount - 1);
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && mLastItemVisible) {
                    onLoad();
                }
            }
        });

        initList();
    }

    private void refreshList(String findData, int findType) {
        listCount = 0;
        initList();
        setList();
        loadList(findData, findType);
    }

    private void loadList(String findData, int findType) {
        Retrofit retrofit;
        IHamonyService hamonyService;
        Call<PostData> post;

        retrofit = new Retrofit.Builder()
                .baseUrl("http://125.176.34.15:9000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hamonyService = retrofit.create(IHamonyService.class);
        post = hamonyService.findData(findData, findType, 0 + listCount);
        post.enqueue(new Callback<PostData>() {
            @Override
            public void onResponse(Response<PostData> response, Retrofit retrofit) {
                switch (response.code()) {
                    case 401:
                        Log.e("search", "end of post");
                        break;

                    case 200:
                        ArrayList<PostData.DataBean> _arrayList = response.body().getData();
                        listCount = _arrayList.size();
                        for (int i = 0; i < _arrayList.size(); i++) {
                            PostData.DataBean array = _arrayList.get(i);
                            Log.e("search", array.getId() + " : " + array.getContent());
                            arrayList.add(array);
                        }
                        adapter.notifyDataSetChanged();
                        break;

                    default:
                        Toast.makeText(getApplicationContext(), R.string.msg_unkown_error, Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("search", t.getMessage());
                Toast.makeText(getApplicationContext(), R.string.msg_network_error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initList() {
        arrayList = new ArrayList<>();
        setList();
    }

    private void setList() {
        adapter = new PostAdapter(getApplicationContext(), arrayList);
        grid_contents.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_image_back: {
                finish();
                break;
            }

            case R.id.search_image_clear: {
                edit_search.setText("");
                break;
            }

            case R.id.search_image_search: {
                onSearch();
                break;
            }

            case R.id.search_relative_title:
            case R.id.search_image_title: {
                onTabChanged();
                break;
            }

            case R.id.search_relative_tag:
            case R.id.search_image_tag: {
                onTabChanged();
                break;
            }
        }
    }

    private void onTabChanged() {
        if (tabPosition == 0) {
            tabPosition = 1;
            image_title.setVisibility(View.INVISIBLE);
            image_tag.setVisibility(View.VISIBLE);
        } else {
            tabPosition = 0;
            image_title.setVisibility(View.VISIBLE);
            image_tag.setVisibility(View.INVISIBLE);
        }
        initList();
        onSearch();
    }

    private void onSearch() {
        String data = edit_search.getText().toString().trim();

        if (data.equals("")) {
            Toast.makeText(getApplicationContext(), R.string.msg_fill_search, Toast.LENGTH_SHORT).show();
            return;
        }

        if (tabPosition == 0) refreshList(data, 1);
        else refreshList(data, 0);
    }

    private void onLoad(){
        String data = edit_search.getText().toString().trim();

        if (data.equals("")) {
            initList();
            Toast.makeText(getApplicationContext(), R.string.msg_fill_search, Toast.LENGTH_SHORT).show();
            return;
        }

        if (tabPosition == 0) loadList(data, 1);
        else loadList(data, 0);
    }
}
