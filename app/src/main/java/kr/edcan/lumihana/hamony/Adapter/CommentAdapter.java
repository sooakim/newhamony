package kr.edcan.lumihana.hamony.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import kr.edcan.lumihana.hamony.Data.CommentData;
import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-07-16.
 */
public class CommentAdapter extends ArrayAdapter<CommentData.DataBean> {
    private Context context;
    private LayoutInflater inflater;
    private int[] color_profile;
    private int color_point;

    public CommentAdapter(Context context, ArrayList<CommentData.DataBean> arrayList) {
        super(context, 0, arrayList);

        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        color_profile = new int[]{R.drawable.background_profile_purple, R.drawable.background_profile_blue, R.drawable.background_profile_brown};
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        if(view!=null) view = convertView;
        else view = inflater.inflate(R.layout.content_comment, null);

        final CommentData.DataBean data = this.getItem(position);

        if(data!= null){
            color_point = new Random().nextInt(color_profile.length);
            TextView text_profile = (TextView) view.findViewById(R.id.comment_text_profile);
            final TextView text_name = (TextView) view.findViewById(R.id.comment_text_name);
            final TextView text_content = (TextView) view.findViewById(R.id.comment_text_contents);
            ImageView image_favorite = (ImageView) view.findViewById(R.id.comment_image_favorite);
            TextView text_num = (TextView) view.findViewById(R.id.comment_text_num);

            if(data.getName()!=null)
            text_profile.setText(data.getName().toString().toUpperCase().trim());
            text_profile.setBackgroundResource(color_profile[color_point]);

            if(data.getName()!=null)
            text_name.setText(data.getName().toString().trim());

            if(data.getContent()!=null)
            text_content.setText(data.getContent().toString().trim());

            if(data.getLike() < 0) text_num.setText(0+"");
            else text_num.setText(data.getLike()+"");

            image_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }

        return view;
    }
}
