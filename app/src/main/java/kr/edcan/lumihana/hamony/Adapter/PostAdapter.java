package kr.edcan.lumihana.hamony.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.BlurTransformation;
import kr.edcan.lumihana.hamony.Activity.CommentActivity;
import kr.edcan.lumihana.hamony.Data.PostData;
import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-07-16.
 */
public class PostAdapter extends ArrayAdapter<PostData.DataBean> {
    private Context context;
    private LayoutInflater inflater;

    public PostAdapter(Context context, ArrayList<PostData.DataBean> arrayList) {
        super(context, 0, arrayList);
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = null;
        
        if(view!= null) view = convertView;
        else view = inflater.inflate(R.layout.content_main, null);
        
        final PostData.DataBean data = this.getItem(position);
        
        if(data != null){
            RelativeLayout main_content = (RelativeLayout) view.findViewById(R.id.main_content);
            ImageView image_background = (ImageView) view.findViewById(R.id.main_image_background);
            TextView text_type = (TextView) view.findViewById(R.id.main_text_type);
            TextView text_comment = (TextView) view.findViewById(R.id.main_text_comment);
            TextView text_content = (TextView) view.findViewById(R.id.main_text_content);

            text_comment.setText(data.getComment() + " / " + 5 + "분전");
            if(data.getTitle()!=null) text_content.setText(data.getTitle().toString().trim());

            Log.e("image", data.getImg()+"");
            Glide.with(context).load("http://125.176.34.15:9000" + data.getImg())
                    .placeholder(R.color.colorPrimary)
                    .skipMemoryCache(false)
                    .bitmapTransform(new BlurTransformation(context, 8))
                    .crossFade()
                    .thumbnail(0.5f)
                    .into(image_background);

            main_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CommentActivity.class);
                    intent.putExtra("id", data.getId());
                    intent.putExtra("title", data.getTitle());
                    intent.putExtra("image", data.getImg());
                    intent.putExtra("isAnonymous", data.isOption());
                    intent.putExtra("content", data.getContent());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

            switch (data.getSelect()) {
                case PostData.TYPE_DEBATE: {
                    text_type.setText(R.string.text_debate);
                    break;
                }

                case PostData.TYPE_GBVOTE : {
                    text_type.setText(R.string.text_gbvote);
                    break;
                }

                case PostData.TYPE_VOTE:{
                    text_type.setText(R.string.text_vote);
                    break;
                }

                default: {
                    text_type.setText(R.string.text_unkown);
                }
            }
        }
        
        return view;
    }
}
