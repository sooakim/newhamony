package kr.edcan.lumihana.hamony.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.Data.VoteData;
import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-07-16.
 */
public abstract class VoteAdapter extends ArrayAdapter<VoteData> {
    private Context context;
    private LayoutInflater inflater;

    abstract public void onDelete(int position);

    public VoteAdapter(Context context, ArrayList<VoteData> arrayList) {
        super(context, 0, arrayList);

        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = null;

        if(view != null) view = convertView;
        else view = inflater.inflate(R.layout.content_vote, null);

        final VoteData data = this.getItem(position);

        if(data != null){
            EditText edit_content = (EditText) view.findViewById(R.id.vote_edit_content);
            ImageView image_delete = (ImageView) view.findViewById(R.id.vote_image_delete);

            edit_content.setText(data.getContent()+"");
            edit_content.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    data.setContent(s.toString().trim());
                }
            });
            
            image_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onDelete(position);
                }
            });

        }

        return view;
    }
}
