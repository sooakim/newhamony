package kr.edcan.lumihana.hamony.Data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by kimok_000 on 2016-07-16.
 */
public class CommentData extends Data{

    /**
     * comment : true
     * data : [{"bulletinid":49,"id":1,"name":"윾머","content":"ㄷㄷ","like":0,"_id":"5789221a6d9a027177691814","__v":0,"likejoin":[],"time":"2016-07-15T17:49:14.262Z"},{"bulletinid":49,"id":2,"name":"윾윾","like":0,"_id":"578922616d9a027177691815","__v":0,"likejoin":[],"time":"2016-07-15T17:50:25.995Z"},{"bulletinid":49,"id":3,"name":"윾윾","like":0,"_id":"578922636d9a027177691816","__v":0,"likejoin":[],"time":"2016-07-15T17:50:27.725Z"},{"bulletinid":49,"id":4,"name":"윾윾","like":0,"_id":"578922646d9a027177691817","__v":0,"likejoin":[],"time":"2016-07-15T17:50:28.488Z"}]
     */

    @SerializedName("comment")
    private boolean comment;
    /**
     * bulletinid : 49
     * id : 1
     * name : 윾머
     * content : ㄷㄷ
     * like : 0
     * _id : 5789221a6d9a027177691814
     * __v : 0
     * likejoin : []
     * time : 2016-07-15T17:49:14.262Z
     */

    @SerializedName("data")
    private ArrayList<DataBean> data;

    public boolean isComment() {
        return comment;
    }

    public void setComment(boolean comment) {
        this.comment = comment;
    }

    public ArrayList<DataBean> getData() {
        return data;
    }

    public void setData(ArrayList<DataBean> data) {
        this.data = data;
    }

    public static class DataBean extends Data{
        @SerializedName("bulletinid")
        private int bulletinid;
        @SerializedName("id")
        private int id;
        @SerializedName("name")
        private String name;
        @SerializedName("content")
        private String content;
        @SerializedName("like")
        private int like;
        @SerializedName("_id")
        private String _id;
        @SerializedName("__v")
        private int __v;
        @SerializedName("time")
        private String time;
        @SerializedName("likejoin")
        private ArrayList<String> likejoin;

        public DataBean(int bulletinid, int id, String name, String content, int like, String _id, int __v, String time, ArrayList<String> likejoin) {
            this.bulletinid = bulletinid;
            this.id = id;
            this.name = name;
            this.content = content;
            this.like = like;
            this._id = _id;
            this.__v = __v;
            this.time = time;
            this.likejoin = likejoin;
        }

        public int getBulletinid() {
            return bulletinid;
        }

        public void setBulletinid(int bulletinid) {
            this.bulletinid = bulletinid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public int getLike() {
            return like;
        }

        public void setLike(int like) {
            this.like = like;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public int get__v() {
            return __v;
        }

        public void set__v(int __v) {
            this.__v = __v;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public ArrayList<String> getLikejoin() {
            return likejoin;
        }

        public void setLikejoin(ArrayList<String> likejoin) {
            this.likejoin = likejoin;
        }
    }
}
