package kr.edcan.lumihana.hamony.Data;

/**
 * Created by kimok_000 on 2016-07-14.
 */
public class OptionData extends Data {
    private String title;
    private String content;

    public OptionData(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
