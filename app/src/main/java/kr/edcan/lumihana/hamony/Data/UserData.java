package kr.edcan.lumihana.hamony.Data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kimok_000 on 2016-07-14.
 */
public class UserData {
    /**
     * id : admin
     * pw : admin
     * email : root@malang.moe
     * name : youngnamkim
     * sex : yx
     * _id : 5776a955c907c6a51a197a72
     * __v : 0
     * joinopinion : null
     */

    @SerializedName("db_RESULT")
    private DataBean db_result;

    public DataBean getDb_result() {
        return db_result;
    }

    public void setDb_result(DataBean db_result) {
        this.db_result = db_result;
    }

    public static class DataBean {
        @SerializedName("id")
        private String id;
        @SerializedName("pw")
        private String pw;
        @SerializedName("email")
        private String email;
        @SerializedName("name")
        private String name;
        @SerializedName("sex")
        private String sex;
        @SerializedName("_id")
        private String _id;
        @SerializedName("__V")
        private int __v;
        @SerializedName("joinopinion")
        private Object joinopinion;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPw() {
            return pw;
        }

        public void setPw(String pw) {
            this.pw = pw;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public int get__v() {
            return __v;
        }

        public void set__v(int __v) {
            this.__v = __v;
        }

        public Object getJoinopinion() {
            return joinopinion;
        }

        public void setJoinopinion(Object joinopinion) {
            this.joinopinion = joinopinion;
        }
    }
}

