package kr.edcan.lumihana.hamony.Data;

/**
 * Created by kimok_000 on 2016-07-16.
 */
public class VoteData extends Data {
    private String content;

    public VoteData(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }
}
