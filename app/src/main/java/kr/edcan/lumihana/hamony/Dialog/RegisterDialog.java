package kr.edcan.lumihana.hamony.Dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import kr.edcan.lumihana.hamony.Data.ErrorData;
import kr.edcan.lumihana.hamony.R;
import kr.edcan.lumihana.hamony.Utils.IHamonyService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by kimok_000 on 2016-07-14.
 */
public class RegisterDialog extends Dialog implements View.OnClickListener {
    private EditText edit_id, edit_pw, edit_pwc, edit_email, edit_name;
    private TextView text_male, text_female, text_register;

    private String id, pw, pwc, email, name, sex = "female";

    public RegisterDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_register);

        edit_id = (EditText) findViewById(R.id.register_edit_id);
        edit_pw = (EditText) findViewById(R.id.register_edit_pw);
        edit_pwc = (EditText) findViewById(R.id.register_edit_pwc);
        edit_email = (EditText) findViewById(R.id.register_edit_email);
        edit_name = (EditText) findViewById(R.id.register_edit_name);
        text_male = (TextView) findViewById(R.id.register_text_male);
        text_female = (TextView) findViewById(R.id.register_text_female);
        text_register = (TextView) findViewById(R.id.register_text_register);

        text_male.setOnClickListener(this);
        text_female.setOnClickListener(this);
        text_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_text_register: {
                onRegister();
                break;
            }

            case R.id.register_text_male: {
                onSex(R.id.register_text_male);
                break;
            }

            case R.id.register_text_female: {
                onSex(R.id.register_text_female);
                break;
            }
        }
    }

    private void onSex(int id) {
        if (id == R.id.register_text_female) {
            text_female.setBackgroundResource(R.drawable.ripple_button_rounded_green);
            text_female.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
            text_male.setBackgroundResource(R.drawable.ripple_edit_rounded_brightgray);
            text_male.setTextColor(getContext().getResources().getColor(R.color.colorDarkerGray));
            sex = "female";
        } else {
            text_male.setBackgroundResource(R.drawable.ripple_button_rounded_green);
            text_male.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
            text_female.setBackgroundResource(R.drawable.ripple_edit_rounded_brightgray);
            text_female.setTextColor(getContext().getResources().getColor(R.color.colorDarkerGray));
            sex = "male";
        }
    }

    private void onRegister() {
        id = edit_id.getText().toString().trim();
        pw = edit_pw.getText().toString().trim();
        pwc = edit_pwc.getText().toString().trim();
        email = edit_email.getText().toString().trim();
        name = edit_name.getText().toString().trim();

        if (id.equals("") || pw.equals("") || pwc.equals("") || email.equals("") || name.equals("")) {
            Toast.makeText(getContext(), R.string.msg_fill_content, Toast.LENGTH_SHORT).show();
            return;
        }

        if (!pw.equals(pwc)) {
            Toast.makeText(getContext(), R.string.msg_pw_not_match, Toast.LENGTH_SHORT).show();
            return;
        }

        registerService();
    }

    private void registerService() {
        final ProgressDialog dialog = new ProgressDialog(getContext());
        dialog.setTitle(R.string.msg_loading);
        dialog.setMessage(getContext().getResources().getString(R.string.msg_now_loading));
        dialog.show();

        Retrofit retrofit;
        IHamonyService hamonyService;
        Call<ErrorData> register;

        retrofit = new Retrofit.Builder()
                .baseUrl("http://125.176.34.15:9000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hamonyService = retrofit.create(IHamonyService.class);
        register = hamonyService.register(id, pw, email, name, sex);
        register.enqueue(new Callback<ErrorData>() {
            @Override
            public void onResponse(Response<ErrorData> response, Retrofit retrofit) {
                switch (response.code()) {
                    case 200:
                        dialog.dismiss();
                        Toast.makeText(getContext(), R.string.msg_helloworld, Toast.LENGTH_SHORT).show();
                        dismiss();
                        break;
                    case 401:
                        Toast.makeText(getContext(), R.string.msg_exist_id, Toast.LENGTH_SHORT).show();
                        break;
                    case 402 :
                        Toast.makeText(getContext(), R.string.msg_exist_name, Toast.LENGTH_SHORT).show();
                        break;
                    case 403 :
                        Toast.makeText(getContext(), R.string.msg_exist_email, Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(getContext(), R.string.msg_unkown_error, Toast.LENGTH_SHORT).show();
                        break;
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("register", t.getMessage());
                Toast.makeText(getContext(), R.string.msg_network_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
