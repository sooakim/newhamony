package kr.edcan.lumihana.hamony.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.Adapter.OptionAdapter;
import kr.edcan.lumihana.hamony.Data.Data;
import kr.edcan.lumihana.hamony.Data.OptionData;
import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-07-14.
 */
public abstract class SelectionDialog extends Dialog{
    private ListView list_option;
    private ArrayList<OptionData> arrayList;
    private OptionAdapter adapter;

    abstract public void onDebate();
    abstract public void onGoodBad();
    abstract public void onVote();

    public SelectionDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_selction);

        list_option = (ListView) findViewById(R.id.selecion_list_options);

        initList();
        addList(new OptionData("1. 자유 발언","주제에 자신의 의견을 글로 표현할 수 있습니다."));
        addList(new OptionData("2. 찬반 토론","투고자가 정한 찬반에 대해 토론할 수 있습니다."));
        addList(new OptionData("3. 투표","투고자가 정한 결정권들을 선택할 수 있습니다."));

        list_option.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0 : onDebate(); break;
                    case 1 : onGoodBad(); break;
                    case 2 : onVote(); break;
                    default: {
                        Toast.makeText(getContext(), "에러", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
            }
        });
    }


    public void initList() {
        arrayList = new ArrayList<>();
        setList();
    }

    public void addList(Data data) {
        arrayList.add((OptionData) data);
        setList();
    }

    public void setList() {
        adapter = new OptionAdapter(getContext(), arrayList);
        list_option.setAdapter(adapter);
    }
}
