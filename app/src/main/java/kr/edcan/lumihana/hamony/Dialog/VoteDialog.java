package kr.edcan.lumihana.hamony.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.Adapter.VoteAdapter;
import kr.edcan.lumihana.hamony.Data.VoteData;
import kr.edcan.lumihana.hamony.R;

/**
 * Created by kimok_000 on 2016-07-17.
 */
public abstract class VoteDialog extends Dialog implements View.OnClickListener{
    private ArrayList<VoteData> arrayList;
    private VoteAdapter adapter;
    private ListView list_contents;
    private TextView text_cancel, text_apply;
    private ImageView image_add;

    abstract public void onVote(ArrayList<VoteData> arrayList);
    abstract public void onCancel();

    public VoteDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);
        setContentView(R.layout.dialog_vote);

        list_contents = (ListView) findViewById(R.id.vote_list_content);
        text_apply = (TextView) findViewById(R.id.vote_text_apply);
        text_cancel = (TextView) findViewById(R.id.vote_text_cancel);
        image_add = (ImageView) findViewById(R.id.vote_image_add);

        initList();
    }

    private void initList(){
        arrayList = new ArrayList<>();

    }

    private void updateList(){
        adapter.notifyDataSetChanged();
    }

    private void setList(){
        adapter = new VoteAdapter(getContext(), arrayList) {
            @Override
            public void onDelete(int position) {
                arrayList.remove(position);
                setList();
            }
        };
        list_contents.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.vote_text_apply : {
                onVote(arrayList);
                break;
            }
            case R.id.vote_text_cancel : {
                onCancel();
                break;
            }
            case R.id.vote_image_add : {
                arrayList.add(new VoteData(""));
                updateList();
                break;
            }
        }
    }
}
