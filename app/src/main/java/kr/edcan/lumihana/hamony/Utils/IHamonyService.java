package kr.edcan.lumihana.hamony.Utils;

import com.squareup.okhttp.RequestBody;

import kr.edcan.lumihana.hamony.Data.CommentData;
import kr.edcan.lumihana.hamony.Data.ErrorData;
import kr.edcan.lumihana.hamony.Data.PostData;
import kr.edcan.lumihana.hamony.Data.UserData;
import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by kimok_000 on 2016-07-14.
 */
public interface IHamonyService {
    int LIST_TYPE_LAST = 1;
    int LIST_TYPE_SPOT = 2;
    int LIST_TYPE_PROGRESS = 3;

    int VOTE_TYPE_GOOD = 0;
    int VOTE_TYPE_BAD = 1;

    int FIND_TYPE_TITLE = 1;
    int FIND_TYPE_TAG = 0;


    @POST("/login")
    @FormUrlEncoded
    Call<UserData> login(
            @Field("id") String id, @Field("pw") String password
    );

    @POST("/signup")
    @FormUrlEncoded
    Call<ErrorData> register(
            @Field("id") String id, @Field("pw") String pw,
            @Field("email") String email, @Field("name") String name, @Field("sex") String sex
    );

    @POST("bulletinlist")
    @FormUrlEncoded
    Call<PostData> listOfPost(@Field("please") int type, @Field("skip") int skip);

    @POST("writebulletin")
    @Multipart
    Call<PostData> writePost(
            @Part("title") String title, @Part("name") String name,  @Part("bulletincontent") String bulletincontent,
            @Part("option") boolean isAnonymous, @Part("bulletinselect") int type,
            @Part("tag") String[] tag, @Part("exittime") long date, @Part("image\"; filename=\"image.jpg\"") RequestBody photo);

    @POST("/writecomment")
    @FormUrlEncoded
    Call<CommentData.DataBean> writeComment(
            @Field("bulletinid") int id, @Field("name") String name, @Field("commentcontent") String content
    );

    @POST("/seecomment")
    @FormUrlEncoded
    Call<CommentData> seeComment(@Field("bulletinid") int id);

    @POST("/vote")
    @FormUrlEncoded
    Call<String> vote(
            @Field("bulletinid") int id, @Field("name") String name,
            @Field("vote") int type
    );

    @POST("/bulletinlist")
    @FormUrlEncoded
    Call<PostData> findData(@Field("finddata") String data, @Field("findtype") int type, @Field("skip") int skip);

}
