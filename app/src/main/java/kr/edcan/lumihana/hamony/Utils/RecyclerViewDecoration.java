package kr.edcan.lumihana.hamony.Utils;

        import android.graphics.Rect;
        import android.support.v7.widget.RecyclerView;
        import android.view.View;

/**
 * Created by kimok_000 on 2016-06-21.
 */
public class RecyclerViewDecoration extends RecyclerView.ItemDecoration {
    private final int mSpace;

    public RecyclerViewDecoration(int space) {
        this.mSpace = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = mSpace;
        outRect.right = mSpace;
        outRect.bottom = mSpace;
        outRect.top = mSpace;
    }
}